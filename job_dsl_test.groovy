#!groovy

out.println("Simple freestyle job creation without anything")
freeStyleJob("jd_simple_freestyle")

out.println("Freestyle job creation with many settings")
0.upto(20, {
    freeStyleJob("jd_not_simple_freestyle" + it) {
        concurrentBuild(true)
        description("Not so simple freestyle job generated via job dsl plugin")
        environmentVariables {
            env("author", "kduchno")
            keepBuildVariables(true)
            keepSystemVariables(true)
            label('slave-node')
            logRotator(-1, 2)
            parameters {
                booleanParam('test_bool', false, 'test boolean parameter')
                stringParam('test_string', 'nothing', 'test string parameter')
            }
            properties {
                buildFailureAnalyzer(true)
            }
            publishers {
                groovyPostBuild {}
                wsCleanup {}
            }
            steps {
                shell()
            }
            wrappers {
                timestamps()
            }
        }
    }
})

// eof
